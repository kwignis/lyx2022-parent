package atqcsz.app.dwd;


import atqcsz.app.BaseSQLApp;
import atqcsz.util.SQLUtil;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;

/**
 * @Author w.k.
 * @Date 2022/8/12 13:56
 * @Version 1.0
 */
public class Kafka2Doris extends BaseSQLApp {
    public static void main(String[] args) {
        new Kafka2Doris().init(2005,1,"kafka2doris",2000L);
    }
    @Override
    protected void handle(StreamExecutionEnvironment env, StreamTableEnvironment tEnv) {
        tEnv.executeSql(
            "create table s_kafka(" +
            "  `@timestamp` string ,  " +
            "  `@metadata` string ,   "+
            "  `beat` string,         "+
            "  `source` string,       "+
            "  `offset` string,       "+
            "  `log` string,          "+
            "  `input` string,        "+
            "  `env` string,          "+
            "  `logsource` string,    "+
            "  `docker` string,       "+
            "  `host` string,         "+
            "  `json` string,         "+
            "  `stream` string,       "+
            "  `prospector` string,   "+
            "  `topic` string " +
   /*             "  )with(" +
                "'connector'='kafka'," +
                "'properties.group.id' = 'kafka2doris'," +
                "'properties.enable.auto.commit'='false'," +
                "'topic'='filebeat-docker-log'," +
                "'properties.bootstrap.servers'='"+ Constant.KAFKA_BROKERS2+"'," +
                "'scan.startup.mode'='earliest-offset'," +
                "'json.fail-on-missing-field' = 'true', " +
                "'json.ignore-parse-errors' = 'false' , " +
                "'format'='json')");*/
                ")"+
                SQLUtil.getKafkaSourceDDL("filebeat-docker-log","kafka2doris145")+
            " ");

        tEnv.executeSql("CREATE TABLE sink_doris(" +
            "  `p_date` date,        " +
            "  `hostname` string,    " +
            "  `time_stamp` string ,  " +
            "  `meta_data` string ,   "+
            "  `beat` string,         "+
            "  `source` string,       "+
            "  `offset` string,       "+
            "  `log` string,          "+
            "  `input` string,        "+
            "  `env` string,          "+
            "  `logsource` string,    "+
            "  `docker` string,       "+
            "  `host` string,         "+
            "  `json` string,         "+
            "  `stream` string,       "+
            "  `prospector` string,   "+
            "  `topic` string " +
            ")with (" +
            "  'connector' = 'doris',\n" +
            "  'fenodes' = '192.168.1.237:18030,192.168.1.236:18030,192.168.1.235:18030',\n" +
            "  'table.identifier' = 'mytest.filebeat_docker_log',\n" +
            "  'username' = 'root',\n" +
            "  'password' = '',\n" +
            "  'sink.properties.two_phase_commit'='true',\n" +
            "  'sink.label-prefix'='doris_demo_emp_001')" +
            "");

        tEnv.executeSql("insert into sink_doris select  " +
            "   cast(DATE_FORMAT(CONVERT_TZ(REGEXP_REPLACE(`@timestamp`,'[A-Z]',' '),'UTC','GMT+08:00'),'yyyy-MM-dd')as date) p_date," +
            "   cast(json_value(`beat`,'$.hostname')as string) as hostname ,"+
            "  `@timestamp` as time_stamp ,  " +
            "  `@metadata` as meta_data  ,   " +
            "  `beat` ,         " +
            "  `source` ,       " +
            "  `offset` ,       " +
            "  `log` ,          " +
            "  `input` ,        " +
            "  `env` ,          " +
            "  `logsource` ,    " +
            "  `docker` ,       " +
            "  `host` ,         " +
            "  `json` ,         " +
            "  `stream` ,       " +
            "  `prospector` ,   " +
            "  `topic`  " +
            " from s_kafka  "
        );


//        tEnv.executeSql("select * from s_kafka").print();

    }
}
