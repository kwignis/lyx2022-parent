package atqcsz.common;

/**
 * @Author w.k.
 * @Date 2022/6/10 11:38
 * @Version 1.0
 */
public class Constant {

    public static final String KAFKA_BROKERS2 = "hadoop102:9092,hadoop103:9092,hadoop104:9092";
    public static final String KAFKA_BROKERS_DEV = "192.168.1.236:9092,192.168.1.235:9092,192.168.1.237:9092";
    public static final String KAFKA_TOPIC_DEV = "demo";

    public static final String KAFKA_BROKERS_SC = "192.168.1.171:31070,192.168.1.172:31071,192.168.1.173:31072";
    public static final String KAFKA_TOPIC_SC = "sdk-log";

    public static final String MYSQL_DRIVER = "com.mysql.cj.jdbc.Driver";

    public static final String TOPIC_ODS_DB = "aaa";

    public static final String DORIS_FE = "192.168.1.235:18030,192.168.1.236:18030,192.168.1.237:18030";
}
