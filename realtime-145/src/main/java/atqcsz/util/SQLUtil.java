package atqcsz.util;


import com.atqcsz.common.Constant;

/**
 * @Author w.k.
 * @Date 2022/6/24 10:41
 * @Version 1.0
 */
public class SQLUtil {
    
    public static String getKafkaSourceDDL(String topic, String groupId) {
        return "with(" +
            " 'connector'='kafka', " +
            " 'properties.bootstrap.servers'='" + Constant.KAFKA_BROKERS_DEV + "', " +
            " 'properties.group.id'='" + groupId + "', " +
            " 'format'='json', " +
            // latest-offset earliest-offset
            " 'scan.startup.mode' = 'latest-offset', " +
            " 'json.fail-on-missing-field' = 'true', " +
            " 'json.ignore-parse-errors' = 'false' , " +
            " 'topic'='" + topic + "' " +
            ")";
    }
    
    public static String getKafkaSinkDDL(String topic) {
        return "with(" +
            " 'connector'='kafka', " +
            " 'properties.bootstrap.servers'='" + Constant.KAFKA_BROKERS_DEV + "', " +
            " 'format'='json', " +
            " 'topic'='" + topic + "' " +
            ")";
    }
    
    public static String getUpsertKafkaSinkDDL(String topic) {
        return "with(" +
            " 'connector'='upsert-kafka', " +
            " 'properties.bootstrap.servers'='" + Constant.KAFKA_BROKERS_DEV + "', " +
            " 'key.format'='json', " +
            " 'value.format'='json', " +
            " 'topic'='" + topic + "' " +
            ")";
    }

    public static String getDorisSinkDDL(String table_identifier) {
        return "with(" +
            "'connector'='doris'," +
            "'fenodes' = '"+ Constant.DORIS_FE +"'," +
            "'table.identifier'='" +table_identifier +"'," +
            "'username' = 'root'," +
            "'password' = ''," +
            "'sink.label-prefix' = 'doris_label'," +
            "'format'='json')";
    }

}
