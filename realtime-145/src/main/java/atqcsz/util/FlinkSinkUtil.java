package atqcsz.util;

import atqcsz.annotation.NoSink;
import org.apache.flink.connector.jdbc.JdbcConnectionOptions;
import org.apache.flink.connector.jdbc.JdbcExecutionOptions;
import org.apache.flink.connector.jdbc.JdbcSink;
import org.apache.flink.connector.jdbc.JdbcStatementBuilder;
import org.apache.flink.streaming.api.functions.sink.SinkFunction;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer;
import org.apache.flink.streaming.connectors.kafka.KafkaSerializationSchema;
import org.apache.kafka.clients.producer.ProducerRecord;

import javax.annotation.Nullable;
import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

/**
 * @Author w.k.
 * @Date 2022/6/14 16:04
 */
public class FlinkSinkUtil {
    public static SinkFunction<String> getKafkaSink(String bs, String topic) {
        
        Properties props = new Properties();
        props.put("bootstrap.servers", bs);
        props.put("transaction.timeout.ms", 15 * 60 * 1000);

        return new FlinkKafkaProducer<String>(
            "default",
            new KafkaSerializationSchema<String>() {

                @Override
                public ProducerRecord<byte[], byte[]> serialize(String element,
                                                                @Nullable Long timestamp) {
                    return new ProducerRecord<>(topic, element.getBytes(StandardCharsets.UTF_8));
                }
            },
            props,
            FlinkKafkaProducer.Semantic.EXACTLY_ONCE
        );
    }

    private static <T> SinkFunction<T> getJdbcSink(String driver,
                                                   String url,
                                                   String userName,
                                                   String password,
                                                   String sql) {
        return JdbcSink.sink(sql,
            (JdbcStatementBuilder<T>) (ps, t) -> {
                // 给占位符赋值! TODO
                // Class.forName  类名.class 对象.getClass
                Class<?> tClass = t.getClass();
                Field[] fields = tClass.getDeclaredFields();
                try {
                    for (int i = 0, position = 1; i < fields.length; i++) {
                        Field field = fields[i];
                        if (field.getAnnotation(NoSink.class) == null) {
                            field.setAccessible(true); // 允许访问私有属性
                            Object v = field.get(t);  // 获取对象中属性的值
                            ps.setObject(position++, v); // 计算器要单独
                        }
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }

            },
            new JdbcExecutionOptions.Builder()
                .withBatchSize(10 * 1024)
                .withMaxRetries(3)
                .withBatchIntervalMs(1000)
                .build(),
            new JdbcConnectionOptions.JdbcConnectionOptionsBuilder()
                .withDriverName(driver)
                .withUrl(url)
                .withUsername(userName)
                .withPassword(password)
                .build()
        );
    }

    

}
